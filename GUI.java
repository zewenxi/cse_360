//Mengqi Wu
//CSE360
// This java file contains the GUI and the driver class of the program.
import javax.swing.border.BevelBorder;
import javax.swing.*;
import java.awt.*; 
import java.awt.event.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.io.*;

public class GUI extends JFrame implements ActionListener
{
	public static final int WIDTH = 1200;
	public static final int HEIGHT = 800;
	
	public static void main(String[] args) 
	{
		GUI gui = new GUI();
		gui.setVisible(true);
	}
	
	JPanel inputpanel, leftpanel, outputpanel, prevpanel, buttonpanel1, buttonpanel2;
	JLabel name, dependencies, duration;
	JTextField nfield, defield, durfield;
	JButton add, run, reset, finish, about, help, showcrit, save;
	JTextArea prev, output;
	JScrollPane scroll1, scroll2;
	ArrayList<Activity> actList = new ArrayList();
	ArrayList<Activity> pathway = new ArrayList();
	ArrayList<ArrayList> pathways = new ArrayList();
	
	public GUI() // gui implementation
	{
		
		
		
		setSize(WIDTH,HEIGHT);
		addWindowListener(new WindowDestroyer());	
		setTitle("CSE360 Group 4 Sample GUI");
		Container f = getContentPane();
		f.setLayout(new GridLayout(1,2));
		leftpanel = new JPanel();
		leftpanel.setLayout(new GridLayout(2,1));
		leftpanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		outputpanel = new JPanel();
		outputpanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		inputpanel = new JPanel();
		inputpanel.setLayout(new GridLayout(4,2,10,20));
		inputpanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		prevpanel = new JPanel();
		prevpanel.setLayout(new FlowLayout());
		prevpanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		buttonpanel1 = new JPanel();
		buttonpanel1.setLayout(new FlowLayout());
		buttonpanel2 = new JPanel();
		buttonpanel2.setLayout(new FlowLayout());
		
		
		//instantiate labels 
		name = new JLabel("Activity Name");
		name.setFont(new Font("Serif", Font.BOLD, 30));
		dependencies = new JLabel("Dependencies");
		dependencies.setFont(new Font("Serif", Font.BOLD, 30));
		duration = new JLabel("Durations");
		duration.setFont(new Font("Serif", Font.BOLD, 30));
		
		//instantiate testfields
		nfield = new JTextField("Please enter a string");
		defield = new JTextField("Please enter activity names, separate by comma");
		durfield = new JTextField("Please enter an integer");

		
		//instantiate button
		add = new JButton("ADD");
		add.setFont(new Font("Serif", Font.BOLD, 25));
		reset = new JButton("RESET");
		reset.setFont(new Font("Serif", Font.BOLD, 25));
		finish = new JButton("FINISH");
		finish.setFont(new Font("Serif", Font.BOLD, 25));
		run = new JButton("RUN");
		run.setFont(new Font("Serif", Font.BOLD, 25));
		about = new JButton("ABOUT");
		about.setFont(new Font("Serif", Font.BOLD, 20));
		help = new JButton("HELP ");
		help.setFont(new Font("Serif", Font.BOLD, 20));
		showcrit = new JButton("Show Critical");
		showcrit.setFont(new Font("Serif", Font.BOLD, 25));
		save = new JButton("SAVE");
		save.setFont(new Font("Serif", Font.BOLD, 25));


		//instantiate testareas
		prev = new JTextArea(20,52);
		prev.setText("This area is a priview of all user inputs.");
		output = new JTextArea(42,50);
		output.setText("This area shows the output.");

		// scroll panel
		scroll1 = new JScrollPane(prev);
		scroll1.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scroll2 = new JScrollPane(output);
		scroll2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
	
		buttonpanel1.add(add);
		buttonpanel2.add(run);
		
		inputpanel.add(name);
		inputpanel.add(nfield);
		inputpanel.add(dependencies);
		inputpanel.add(defield);
		inputpanel.add(duration);
		inputpanel.add(durfield);
		inputpanel.add(buttonpanel1);
		inputpanel.add(buttonpanel2);

		
		prevpanel.add(scroll1);
		prevpanel.add(about);
		prevpanel.add(Box.createRigidArea(new Dimension(60,0)));
		prevpanel.add(help);
		
		
		leftpanel.add(inputpanel);
		leftpanel.add(prevpanel);
		
		outputpanel.add(scroll2);
		outputpanel.add(showcrit);
		outputpanel.add(save);
		outputpanel.add(reset);
		//outputpanel.add(Box.createRigidArea(new Dimension(60,60)));
		outputpanel.add(finish);
		

		f.add(leftpanel);
		f.add(outputpanel);
		
		help.addActionListener(this);
		about.addActionListener(this);
		add.addActionListener(this);
		run.addActionListener(this);
		reset.addActionListener(this);
		finish.addActionListener(this);
		showcrit.addActionListener(this);
		save.addActionListener(this);

	}
	
	public void actionPerformed(ActionEvent e) // button listener implementation
	{
		
		if(e.getActionCommand().equals("HELP "))
		{
			JOptionPane.showMessageDialog(null, "1.Enter the activity name, dependencies and duration into the corresponding text fields.\n"
				+ "2.Make sure the activity name must be characters, dependencies are activity names (enter \"none\" if not applicable), and duration is an integer, otherwise the error message will appear on the corresponding text field.\n"
				+ "3.Click ADD button to add current input.\n"
				+ "4.Repeat step 1-3 until you finish entering all inputs.\n"
				+ "5.Click RUN button to run the program, the output will appear on the output text area.\n"
				+ "6.Click RESET to restart the program or click FINISH to quit the program.\n"
				+ "7.Click SAVE to save to file (default location: disk E)\n"
				+ "8.Click Show Critical to get the critical path.\n"
				+ "9.To change the duration of an Activity, just reenter the new Activity information with the correct duration then click add.\n", "Help", 1);
			
		}
		else if(e.getActionCommand().equals("ABOUT")) // about button function
		{
			JOptionPane.showMessageDialog(null, "The program is to analyze a network diagram and determine all paths in the network.\nProducer: Brain Bruns, Kyle Calnimptewa, Mengqi Wu, Zewen Xi.\r\n", "About", 1);
		}
		else if(e.getActionCommand().equals("ADD")) // add button function
		{
			nfield.setForeground(Color.black);
			defield.setForeground(Color.black);
			durfield.setForeground(Color.black);
			boolean a = false, b = false, c = false;
			Activity newActivity = new Activity();
			String name = nfield.getText();
			if(notString(name)) // check name input
			{
				nfield.setForeground(Color.red);
				nfield.setText("!!! Invalid Input, Please Check and Enter Again !!!");
				
			}
			else {
				newActivity.setName(name);
				a = true;
			}
			String dependency = defield.getText();
// need to implement the dependency separte by comma!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!			
			if(notString(dependency)) // check dependency input
			{
				defield.setForeground(Color.red);
				defield.setText("!!! Invalid Input, Please Check and Enter Again !!!");
																																	
				
			}
			else
			{
				newActivity.setDependency(dependency);
				b = true;
			}
			String dur = durfield.getText();
			if(notInt(dur)) // check duration input
			{
				durfield.setForeground(Color.red);
				durfield.setText("!!! Invalid Input, Please Check and Enter Again !!!");
				
			}
			else
			{
				int duration = Integer.parseInt(dur);
				newActivity.setDuration(duration);
				c = true;
			}
			
			if(a && b && c)
			{
				//implement to see if the new activity name exist.
				if(searchlist(newActivity.getName()))
				{
					int reply = JOptionPane.showConfirmDialog(null, "The activity is already exist, do you want to replace it?", "Replace", JOptionPane.YES_NO_OPTION);
					if(reply == JOptionPane.YES_OPTION)
					{
						replaceDur(newActivity.getName(),newActivity.getDuration());
						
					}
				}
				else 
				{
					
					actList.add(newActivity);
					prev.append(newActivity.toString());
					
				}
			}
		
			
			
		}
		else if(e.getActionCommand().equals("RUN"))
		{
			pathway = new ArrayList();
			pathways = new ArrayList();
			
			if(actList.size() > 0)
			{
				if(findnone() > 1)
				{
					output.setText("Error!! There are nodes that are not connected.");
					return;
				}
				try {
					Recursion(findfirst());
					caldur(); // calculate duration
					sortpath();
					
					output.setText(" ");
					for(int i = 0; i < pathways.size(); i++)
					{
						
							
							output.append("\r\n");
							for(int j = 0; j<pathways.get(i).size(); j++)
							{
								
								output.append(((Activity) pathways.get(i).get(j)).tooutput());
								
							}			
					}
				}
				catch(StackOverflowError ex)
				{
					output.setText("Error!! There is a loop in the input!");
				}
				
			}

		}
		else if(e.getActionCommand().equals("SAVE"))
		{
			String reply = JOptionPane.showInputDialog(null, "Please indicate the file name: ", "Save to File", JOptionPane.OK_OPTION);
			// implement file IO
			String content = "Title:\r\n"
									+ "\tActivity & Paths Information\r\n"
							+"\r\nDate and Time of Creation:\r\n"
									+ "\t" + getTime() + "\r\n"
							+"\r\nList of all activities in alphanumeric order with current duration:\r\n"
									+ sortAct() + "\r\n"
							+"\r\nList of all paths with the activity names and total duration:\r\n"
									+ "\t" + output.getText();
			try
			{
				BufferedWriter writer = new BufferedWriter(new FileWriter("E:\\"+reply +".txt"));
				writer.write(content);
				writer.close();
				
			}catch (IOException e1)
			{
				e1.printStackTrace();
				
			}
			
		
		}

		else if(e.getActionCommand().equals("FINISH"))
		{
			System.exit(0);
		}
		else if(e.getActionCommand().equals("RESET"))
		{
			actList = new ArrayList();
			pathway = new ArrayList();
			pathways = new ArrayList();
			output.setText("This area shows the output");
			prev.setText("This area is a priview of all user inputs.");
			nfield.setText("Please enter a string");
			defield.setText("Please enter activity names, separate by comma");
			durfield.setText("Please enter an integer");

		}
		else if(e.getActionCommand().equals("Show Critical"))
		{
			// need to implement the show critical path function.
			ArrayList<ArrayList> critPaths = new ArrayList();
			String endAct1, endAct2;
			int dur1, dur2;
			boolean shouldadd = true;
			for(int i = 0; i < pathways.size(); i++)
			{
				endAct1 = ((Activity) pathways.get(i).get(pathways.get(i).size()-2)).getName();
				dur1 = ((Activity) pathways.get(i).get(pathways.get(i).size()-1)).getDuration();
				if(critPaths.isEmpty())
				{
					critPaths.add(pathways.get(i));
				}
				else
				{
					for(int j = 0; j < critPaths.size(); j++)
					{
						
						endAct2 = ((Activity) critPaths.get(j).get(critPaths.get(j).size()-2)).getName();
						dur2 = ((Activity) critPaths.get(j).get(critPaths.get(j).size()-1)).getDuration();
						if(endAct1.equals(endAct2))
						{
							if(dur1 > dur2)
							{
								critPaths.remove(j);
							}
							else if (dur1 < dur2)
							{
								shouldadd = false;
							}
						}
						else
						{
							shouldadd = true;
						}
					}
					
					if(shouldadd)
					{
						critPaths.add(pathways.get(i));
					}
					
					
				
				}
					
					
					
			}
				
		
			output.setText("Critical Paths: \n");
			for(int i = 0; i < critPaths.size(); i++)
			{
				
					
					output.append("\r\n");
					for(int j = 0; j<critPaths.get(i).size(); j++)
					{
						
						output.append(((Activity) critPaths.get(i).get(j)).tooutput());
						
					}			
			}
			

		}
		
		
	}
	
	// method to check if the input is characters, return true if not
	public boolean notString(String str)
	{
		char[] chars = str.toCharArray();
		for(int i = 0; i < str.length(); i++)
		{
			if(Character.isLetter(chars[i]) || chars[i] == ',')
			{
				
			}
			else
			{
				return true;
			}
		}
		
		return false;
	}
	
	// method to check if the input is all digits, return true if not
	public boolean notInt(String num)
	{
		try
		{
			Integer.parseInt(num);
		}
		catch(NumberFormatException ex)
		{
			return true;
		}
		return false;
	}
	
	
	public ArrayList<Activity> finddec(String name) // This method take a String as parameter, and returns an ArrayList with activities, whose dependency is equal to the given string.
	{
		ArrayList<Activity> decs = new ArrayList();
		for(int i = 0; i < actList.size(); i++)
		{
			if(actList.get(i).getDependency().contains(name))
			{
				decs.add((Activity) actList.get(i));
			}
		}
		
		return decs;
	}
	

	// method to find the first Activity and returns it.
	public Activity findfirst()
	{
		Activity aaa = new Activity();
		for(int i = 0; i < actList.size(); i++)
		{
			if(actList.get(i).getDependency().equalsIgnoreCase("none"))
			{
				aaa = actList.get(i);
			}
		}
		return aaa;
	}
	
	public int findnone()
	{
		int result = 0;
		for(int i = 0; i < actList.size(); i++)
		{
			if(actList.get(i).getDependency().equalsIgnoreCase("none"))
			{
				result += 1;
			}
		}
		
		return result;
	}
	
	// method to calculate durations of the path.
	public void caldur()
	{
		int dur;
		for(int i = 0; i < pathways.size(); i++)
		{
			dur = 0;
			for(int j = 0; j<pathways.get(i).size(); j++)
			{
				dur += ((Activity) pathways.get(i).get(j)).getDuration();
							
			}	
			Activity dura = new Activity();
			dura.setName("Total Duration: " + Integer.toString(dur));
			dura.setDuration(dur);
			pathways.get(i).add(dura);
		}
		
	}
	
	
	// method to sort path based on the duration in decreasing order
	public void sortpath()
	{
		for(int i = 0; i < pathways.size()-1; i++)
		{
			int max = i;
			for(int j = i+1; j < pathways.size(); j++ )
			{
				if((( (Activity) pathways.get(j).get(pathways.get(j).size()-1)).getDuration() > ((Activity) pathways.get(max).get(pathways.get(max).size()-1)).getDuration()))
				{
					max = j;												
				}
			}
			
			ArrayList temp = pathways.get(max);
			pathways.set(max,new ArrayList(pathways.get(i)));
			pathways.set(i, new ArrayList(temp));
		}
	}
	
	// sort input activity
		public String sortAct()
		{
			Character c1, c2;
			String res = "";
			for(int i = 0; i < actList.size()-1; i++)
			{
				int min = i;
				for(int j = i+1; j < actList.size(); j++ )
				{
					c1 = actList.get(j).getName().charAt(0);
					c2 = actList.get(min).getName().charAt(0);
					if(c1.compareTo(c2) <= 0)
					{
						min = j;												
					}
				}
				
				Activity temp = actList.get(min);
				actList.set(min,actList.get(i));
				actList.set(i, temp);
			}
			
			for(int i = 0; i < actList.size(); i++)
			{
				res += "\r\n\t" + (actList.get(i).getName() + "\t" + Integer.toString(actList.get(i).getDuration()));
			}
			
			return res;
		}
	

	// method to recursively create path.
	public void Recursion(Activity a)
	{
		if(finddec(a.getName()).size() == 0)
		{
			pathway.add(a);
			if(pathway.get(0) != findfirst() && pathway.size() > 0) //first element 
			{
				pathway.add(0, findfirst());
			}
			pathways.add(pathway);
			pathway = new ArrayList();
			return;
			
		}
		else
		{
			for(int i = 0; i < finddec(a.getName()).size(); i++)
			{
				Activity aa = new Activity();
				aa = finddec(a.getName()).get(i);
				pathway.add(a);
				Recursion(aa);		
			}
		}
	}
	
	public boolean searchlist(String name)
	{
		
		for(int i = 0; i < actList.size(); i++)
		{
			if(actList.get(i).getName().equals(name))
			{
				return true;
			}
		}
		
		
		return false;
	}
	
	public void replaceDur(String name, int dur)
	{
		
		for(int i = 0; i < actList.size(); i++)
		{
			if(actList.get(i).getName().equals(name))
			{
				actList.get(i).setDuration(dur);
			}
		}
	}
	
	public String getTime()
	{
		String res;
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
		LocalDateTime time = LocalDateTime.now();
		res = dtf.format(time);
		return res;
		
	}
	

}

// method to close window properly
 class WindowDestroyer extends WindowAdapter
{
	public void windowClosing(WindowEvent e)
	{
		System.exit(0);
	}
}