import java.util.ArrayList;

//Mengqi Wu
// This java file contains the Activity class
public class Activity {
	private String name;
	private String dependency;
	private int duration;
	private int id;
	
	public Activity()
	{
		name = new String("N/A");
		dependency = new String("N/A");
		duration = 0;
		id = 0;
		//predecessor = new ArrayList();
		//successor = new ArrayList();
		
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getDependency()
	{
		return dependency;
	}
	
	public int getDuration()
	{
		return duration;
	}
	
	public void setName(String aName)
	{
		name = aName;
	}
	
	public void setDependency(String newDep)
	{
		dependency = newDep;
	}
	
	public void setDuration(int aduration)
	{
		duration = aduration;
	}
	
	public String toString()
	{
		String result = "\r\nActivity Name: " + name + "   Dependencies: " + dependency + "   Duration: " + Integer.toString(duration) + "\r\n";
		return result;
	}
	

	
	public String tooutput()
	{
		String result = name + "->";
		return result;
	}
}
